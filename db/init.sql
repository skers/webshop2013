-- Resetta dbn
drop database douchedata;
create database douchedata;
use douchedata;

-- Tables
CREATE TABLE User (
   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   username char(20) NOT NULL,
   password char(20) NOT NULL,
   realname char(50)
);

CREATE TABLE Item (
   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   name char(100) NOT NULL,
   description char(255),
   price char(10),
   stock INT NOT NULL
);

CREATE TABLE PurchaseOrder (
   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   user INT NOT NULL,
   FOREIGN KEY (user) REFERENCES User(id)
);


CREATE TABLE Purchase (
   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   user INT,
   purchaseorder INT NOT NULL,
   item INT NOT NULL,
   FOREIGN KEY (purchaseorder) REFERENCES PurchaseOrder(id),
   FOREIGN KEY (item) REFERENCES Item(id),
   FOREIGN KEY (user) REFERENCES User(id)
);

prepare pwauth from 'select * from User where username = ? and password = ?';
