-- fyll på med godtycklig data
insert into User(username, realname, password) values('davve', 'David Wikmans', 'trololo');
insert into User(username, realname, password) values('simon', 'Simon Kers', 'simon');
insert into User(username, realname, password) values('pinne', 'Simon Kers', 'pinne');
insert into Item(name, description, price, stock) values(
     'Ray Ban Aviator Sunglasses'
   , 'Are you Cruise or Goose? Settle it once and for all with these fine glasses.'
   , '14888'
   , 100
);

insert into Item(name, description, price, stock) values(
   'Ed Hardy Popped Collar Shirt'
   , 'This fine t-shirt from Ed Hardy comes with the collar pre-popped'
   , '14888.99'
   , 30
);

insert into Item(name, description, price, stock) values(
   'Douche bag'
   , 'Going back to the roots with this one, enjoy the benefits of a medical grade douche bag while exploring the origins of our culture'
   , '49.95'
   , 1000
);

insert into Item(name, description, price, stock) values(
   'Tank top'
   , 'Classic wifebeater in polyester accentuating the pecs of the wearer.'
   , '39.95'
   , 1000
);

insert into Item(name, description, price, stock) values(
   'MicroSoft Zune'
   , 'Portable music player from MicroSoft, now featuring the Bob user interface'
   , '1000.00'
   , 7
);

insert into Item(name, description, price, stock) values(
     'JAS 39 Gripen'
   , 'Roll with style in this military grade (we are serious bout this one folks) aircraft, essential to a bros swagger'
   , '580000000.00'
   , 3
);

insert into Item(name, description, price, stock) values(
     'CD: DJ Oizo - Douche Beat'
   , 'UNTZ all night long'
   , '5.95'
   , 243
);

insert into Item(name, description, price, stock) values(
     'CD: PSY - Gangnam Style'
   , 'This song is cool but we do not know why yet'
   , '5.95'
   , 383
);

insert into Item(name, description, price, stock) values(
     'CD: Rihanna - Good girl gone bad'
   , 'Good Girl Gone Bad is the third studio album by Barbadian R&B singer Rihanna'
   , '5.95'
   , 147
);

insert into Item(name, description, price, stock) values(
     'CD: Swedish House Mafia - Until One'
   , 'Until One is the first studio album by swedish house music group swedish house mafia. it was released on 22 october 2010.'
   , '5.95'
   , 35
);

insert into Item(name, description, price, stock) values(
     'CD: LMFAO - Party Rock'
   , 'LMFAO began their career performing as part of the electro club scene in Los Angeles, which at the time featured DJ/producers like Steve Aoki and Adam Goldstein.'
   , '5.95'
   , 930
);

-- så här görs en beställning, som en transaktion? argument blir användarnamn med en lista med artiklar + antal
insert into PurchaseOrder(user) (select User.id from User where username = 'davve');
SELECT @user := LAST_INSERT_ID();

--SELECT @item := Item.id from Item where description = 'Pikétröja rosa';
--insert into Purchase(purchaseorder,quantity,item) select @user,10,Item.id from Item where description='Pikétröja rosa';
--update Item set stock = stock -10 where id = @item;
--
--SELECT @item := Item.id from Item where description = 'Pilotsolglasögon';
--insert into Purchase(purchaseorder,quantity,item) select @user,1,@item;
--update Item set stock = stock -1 where id = LAST_INSERT_ID();



-- här ska finurliga statements in som visar ändrar och redigerar data
--
--
--
