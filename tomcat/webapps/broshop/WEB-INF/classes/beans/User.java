package beans;
import java.io.Serializable;

public class User implements java.io.Serializable {

	private int id;
	private String username, realname, password;

	public User() {}

	public int getID() { return this.id; }
	public void setID(int id) { this.id = id; }

	public String getUsername() { return this.username; }
	public void setUsername(final String name) { this.username = name; };

	public String getRealname() { return this.realname; }
	public void setRealname(final String name) { this.realname = name; };

	public String getPassword() { return this.password; }
	public void setPassword(final String name) { this.password = name; };
}