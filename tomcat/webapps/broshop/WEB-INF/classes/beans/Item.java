/**********************************************
* Item model                                  *
**********************************************/
package beans;
import java.io.Serializable;

public class Item implements java.io.Serializable {

	private int id, stock, price;
	private String name, description;

	public Item() {}

	public int getID() { return this.id; }
	public void setID(int id) { this.id = id; }
	public int getStock() { return this.stock; }
	public void setStock(int q) { this.stock = q; }
	public int getPrice() { return this.price; }
	public void setPrice(int price) { this.price = price; }
	public String getDescription() { return this.description; }
	public void setDescription(final String name) { this.description = name; };
	public String getName() { return this.name; }
	public void setName(final String name) { this.name = name; };
}