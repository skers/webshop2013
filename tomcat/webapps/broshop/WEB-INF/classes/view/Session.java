/***********************************************
 * Servlet to act as intermediary for database  *
 * and tomcat sessions. Get returns the current *
 * set of data. POST is used for adding and     *
 * removing from the users shopping basket      *
 ***********************************************/
package view;
import bo.Business;
import bo.Listener;
import beans.Item;

import java.util.ArrayList;
import java.io.IOException;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Session extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Business bo = null;

	public void init() throws ServletException {
		bo = Listener.bo;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession(false);
		RequestDispatcher view;

		request.getSession().removeAttribute("items");
		session.setAttribute("items",bo.getItems());

		view = request.getRequestDispatcher("index.jsp");
		view.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		final String item_add = request.getParameter("add");
		final String item_rm = request.getParameter("rm");
		final String item_buy = request.getParameter("buy");
		HttpSession session = request.getSession(false);
		RequestDispatcher view = request.getRequestDispatcher("index.jsp");

		ArrayList<String> cart = (ArrayList<String>) session.getAttribute("cart");

		if (item_add != null) {
			cart.add(item_add);
		}

		else if (item_rm != null) {
			cart.remove(item_rm);
			view = request.getRequestDispatcher("cart.jsp");
		}

		else if (item_buy != null) {
			String user = (String) session.getAttribute("username");
			if (user != null) {
				bo.purchase(user, cart);
				request.getSession().removeAttribute("items");
				cart.clear();
				System.out.println("attempting purchase");
			}
		}

		session.setAttribute("cartsize",cart.size());
		view.forward(request, response);
	}
}
