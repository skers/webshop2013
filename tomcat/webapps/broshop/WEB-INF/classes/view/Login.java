/*************************************************
 * Servlet to authorize and log out users.        *
 * POST for authorization and GET for logging out *
 *************************************************/
package view;
import bo.Business;
import bo.Listener;

import java.io.IOException;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Business bo = null;

	public void init() throws ServletException {
		bo = Listener.bo;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession(true);

		if (bo.authenticate(request.getParameter("username"), request.getParameter("password"))) {
			session.setAttribute("username", request.getParameter("username"));
		}

		RequestDispatcher view = request.getRequestDispatcher("index.jsp");
		view.forward(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession(true);
		final String instruction = request.getParameter("do");
		if (instruction != null)
			if (instruction.toLowerCase().equals("logout")) 
			{
				session.invalidate();
				RequestDispatcher view = request.getRequestDispatcher("index.jsp");
				view.forward(request, response);
			}
	}
}
