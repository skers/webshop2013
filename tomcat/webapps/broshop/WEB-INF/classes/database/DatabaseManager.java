package database;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.*;
import java.util.ArrayList;
import beans.Item;
import beans.User;
public class DatabaseManager {
	DataSource pool = null;
	Connection conn = null;
	Statement stmt = null;

	public DatabaseManager() {
		try {
			InitialContext ctx = new InitialContext();
			pool = (DataSource)ctx.lookup("java:comp/env/jdbc/brodb");
		} catch (NamingException ex) {
			ex.printStackTrace();
		}
	}

	public ArrayList<Item> getItems() {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			conn = pool.getConnection();
			stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery("select * from Item");
			int count = 0;
			while (rset.next()) {
				Item i = new Item();
				i.setID(rset.getInt("id"));
				i.setName(rset.getString("name"));
				i.setPrice(rset.getInt("price"));
				i.setStock(rset.getInt("stock"));
				i.setDescription(rset.getString("description"));
				items.add(i);
				count++;
			}
			System.out.println("Added " + count + " items to cache");
		} catch (SQLException e) { e.printStackTrace(); }
		finally {
			try {
				if (stmt != null) stmt.close();
				if (conn != null) conn.close();  
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return items;
	}

	public boolean authenticate(String user, String pass) {
		final String verifypasswd = "select * from User where username = ? and password = ?";
		PreparedStatement p_stmt = null;
		boolean authorized = false;

		try {
			conn = pool.getConnection();

			p_stmt = conn.prepareStatement(verifypasswd);
			p_stmt.setString(1, user);
			p_stmt.setString(2, pass);

			ResultSet rset = p_stmt.executeQuery();
			int count=0;
			while(rset.next()) {
				count++;
			}
			
			// User found
			if (count > 0) {
				authorized = true;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (p_stmt != null) p_stmt.close();
            if (conn != null) conn.close();  // return to pool
        } catch (SQLException ex) {
        	ex.printStackTrace();
        }
    }
    return authorized;
}


public void purchase(ArrayList<String> list, String username) {
	PreparedStatement preparedStatementInsert = null;
	PreparedStatement preparedStatementUpdate = null;
	Statement stmt = null;
	boolean authorized = false;
	String insertTableSQL = "INSERT INTO PurchaseOrder(user) (select User.id from User where username ='" + username + "')";
	String decrementStock = "update Item set stock = stock - 1 where id = ?";
	String purchaseSQL = "insert into Purchase(purchaseorder, user, item) values(?, ?, ?)";

	try {
		conn = pool.getConnection();
		conn.setAutoCommit(false);
		preparedStatementInsert = conn.prepareStatement(insertTableSQL, PreparedStatement.RETURN_GENERATED_KEYS );
		
		//preparedStatementInsert.executeUpdate();
		preparedStatementInsert.executeUpdate();
		ResultSet keys = preparedStatementInsert.getGeneratedKeys();
		int order = -1;
		while (keys.next())
			order = keys.getInt(1);
		
		stmt = conn.createStatement();
		int user = -1;
		ResultSet targetUser = stmt.executeQuery("select * from User where username = '" + username + "'");
		while (targetUser.next())
			user = targetUser.getInt("id");
		//System.out.println(user);

		for (int i = 0; i < list.size(); i++) {
			System.out.println("purchasing: " + list.get(i));
			preparedStatementInsert = conn.prepareStatement(purchaseSQL);
			preparedStatementInsert.setInt(1,order);
			preparedStatementInsert.setInt(2,user);
			preparedStatementInsert.setInt(3,Integer.parseInt(list.get(i)));
			preparedStatementInsert.executeUpdate();

			preparedStatementInsert = conn.prepareStatement(decrementStock);
			preparedStatementInsert.setInt(1,Integer.parseInt(list.get(i)));
			preparedStatementInsert.executeUpdate();
			
		}

		conn.commit();

	} catch (SQLException ex) {
		ex.printStackTrace();
	} finally {
		try {
            if (conn != null) conn.close();  // return to pool
        } catch (SQLException ex) {
        	ex.printStackTrace();
        }
    }
}
}