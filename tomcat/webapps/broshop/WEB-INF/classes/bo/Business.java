package bo;

import java.util.ArrayList;
import database.DatabaseManager;
import beans.Item;
import beans.User;
public class Business {
	DatabaseManager dbm = null;
	ArrayList<Item> items = null;
	ArrayList<User> users = null;
	public Business() {
		dbm = new DatabaseManager();
		items = new ArrayList<Item>();
		items = dbm.getItems();
	}

	private void refreshCache() {
		items.clear();
		items = dbm.getItems();
	}

	User getUser(String username) {
		for (int i = 0; i < users.size(); i++)	{
			if (users.get(i).getUsername().equals(username))
				return users.get(i);
		}
		return null;
	}

	boolean setUser(User u) {
		for (int i = 0; i < users.size(); i++)	{
			if (users.get(i).getUsername().equals(u.getUsername()))
				users.set(i, u);
			return true;
		}
		return false;	
	}

	public ArrayList<Item> getItems() { return items; };
	public ArrayList<User> getUsers() { return users; };
	public boolean authenticate(String user, String pass) { return dbm.authenticate(user, pass); };
	public void purchase(String user, ArrayList<String> c) { dbm.purchase(c, user); refreshCache(); };
}