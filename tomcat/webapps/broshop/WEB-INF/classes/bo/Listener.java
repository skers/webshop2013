package bo;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import bo.Business;

public class Listener implements ServletContextListener {
	public static Business bo = null;

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("ServletContext started");
		bo = new Business();
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("ServletContext destroyed");
		bo = null;
	}
}
