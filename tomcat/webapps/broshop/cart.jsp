<%@ page import="java.util.ArrayList" %>
<%@ page import="java.lang.Integer" %>
<%@ page import="beans.Item" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Bro shop - get yo gear here, ya hear?</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
  <link href="css/united.css" rel="stylesheet">
  <style type="text/css">
  body {
    padding-top: 60px;
    padding-bottom: 40px;
  }
  .sidebar-nav {
    padding: 9px 0;
  }
  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">

  <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <jsp:include page="navbar.jsp" />

    <div class="container-fluid">
      <div class="row-fluid span12">
        <div class="span12">
          <div class="row-fluid">

  <h2>Shopping cart</h2>

<%  ArrayList<Item> items = (ArrayList<Item>) session.getAttribute("items");
    ArrayList<String> cart = (ArrayList<String>) session.getAttribute("cart");
    int sum = 0; %>
<% if (session.getAttribute("items") != null && session.getAttribute("cart") != null)
  for (int c = 0; c < cart.size(); c++) 
    for (int x = 0; x < items.size(); x++) 
      if (Integer.parseInt(cart.get(c)) == items.get(x).getID()) 
      //if (items.get(x).getID() == Integer.parseInt(cart.get(c))) 
        { sum += items.get(x).getPrice(); %>

<!-- Insert new row -->

<div class="span3 well well-small">
  <h4>
    <%= items.get(x).getName() %>
  </h4>
    <p style="text-align:right">
      &euro;<%= items.get(x).getPrice() %>
    </p>
  <form action="Session.do" method="POST">
    <button type="submit" name="rm" value=<%= items.get(x).getID() %> class="btn btn-success btn-small pull-right" href="#">Remove from cart</button>
  </form>
  
</div>
<% } %>
</div> <!-- row-fluid -->
<br><br>
<div class="container-fluid">
<h3>Please proceed</h3>
<h4>Total: $<%= sum %> </h4> 
<a href="/broshop/" class="btn btn-large btn-warning">Cancel</a>
<form action="Session.do" method="POST">
<button type "submit" name="buy" value="mjau" class="btn btn-large btn-success">Checkout >></button>
</form>
<br><br><br>
          </div>
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; Douche Data 2012</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
  </html>

