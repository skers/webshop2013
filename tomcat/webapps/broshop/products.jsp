<%@ page import="java.util.ArrayList" %>
<%@ page import="beans.Item" %>

<div class="row-fluid">
  <h2>Products</h2>

<% ArrayList<Item> items = (ArrayList<Item>) session.getAttribute("items"); %>
<% if (session.getAttribute("items") != null) for (int x = 0; x < items.size(); x++) { %>

<!-- Insert new row -->

<div class="span3 well well-tiny">
  <h4>
    <%= items.get(x).getName() %>
    <small style="text-align:right">
      &euro;<%= items.get(x).getPrice() %>
    </small>
  </h4>
  <p><%= items.get(x).getDescription() %></p>
  <form action="Session.do" method="POST">
    <button type="submit" name="add" value=<%= items.get(x).getID() %> class="btn btn-success btn-small pull-right" href="#">Add to cart</button>
  </form>
  <p>
  <span class="label">
    <%= items.get(x).getStock() %> left
  </span>
  </p>
</div>
<% } %>
</div> <!-- row-fluid -->
