    <div class="navbar  navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="/broshop/">Bro$hop</a>
          <div class="nav-collapse collapse">

            <ul class="nav">
              <li class="active"><a href="/broshop/">Products</a></li>
              <li><a href="/broshop/contact.jsp">About</a></li>
              <li><a href="/broshop/contact.jsp">Contact</a></li>
            </ul>
            <% if (session.getAttribute("username") != null) { %>
          <div class="navbar-search pull-right" style="padding-top:5px">
            <%= session.getAttribute( "username" ) %>
            <a href="Login.do?do=logout" class="navbar-link">log out</a>
          </div>
            <% } else { %> 
            <form METHOD="POST" ACTION="Login.do" class="navbar-search form-inline pull-right">
              <input class="input-small" style="height:16px" type="text" name=username placeholder="username">
              <input class="input-small" style="height:16px" type="password" name=password placeholder="password">
              <label class="checkbox">
                <input type="checkbox"> register
              </label>
              <button class="btn btn-mini" type="submit">Go</button>
            </form>
            <% } %>
            <div class="navbar navbar-search pull-right" style="padding-right:15px; padding-top:5px">
            <a href="cart.jsp">Cart (<%= session.getAttribute("cartsize") == null ? 0 : session.getAttribute("cartsize") %>)</a>  <!--- lägger till antal efter att ha fixat shoppingcart -->
          </div>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
 
