#!/bin/sh
#set -x

# Create db before trying anything, instructions in README, files in ./db/

# Compile Java
cd ./tomcat/webapps/broshop/WEB-INF/classes/
make
cd -
# Run Java
sudo ./tomcat/bin/catalina.sh run
cd -
# Delete class files The files seems to stick so, the script issues the
# remove command 6 seconds after exit.
(sleep 6 && sudo find . -type f -name "*.class" -exec rm -f {} \;) &
